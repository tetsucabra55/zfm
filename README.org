#+title: README For ZFM
* Dependencies
  To run zfm, rofi and the fish shell are needed.
* To Install

  Add '~/zmf/' to $PATH

  OR

  Run

  #+begin_src bash
  cd zfm
  sudo cp zfm /bin
  #+end_src

* Code Documentation

** Mark as a Fish File

  #+begin_src fish :tangle zfm

    #!/bin/fish

  #+end_src

** Main Loop for the program

  #+begin_src fish :tangle zfm

    while true

  #+end_src

*** Find a File to Open or Close ZFM
   #+begin_src fish :tangle zfm
       set file (ls -a $PWD | rofi -dmenu -i -p "$PWD")
       if [ $file = "exit" ]
           break
       end
   #+end_src

*** Choose a Program to Open
   #+begin_src fish :tangle zfm
       set program (ls /bin/ | rofi -dmenu -p "Execute" -i)
   #+end_src

*** Open in the Program or Reopen in ZFM
   #+begin_src fish :tangle zfm
       if [ $program = "zfm" ]
           cd $file
           continue

       else if [ $fileOrFolder = "exit" ]
           break

       else
           $program $file
           break
       end
   #+end_src
*** End Loop
   #+begin_src fish :tangle zfm
     end
   #+end_src
